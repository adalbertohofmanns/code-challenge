# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
puts '############################# Clients #################################'
20.times do |c|
  Client.create(name: "Cliente #{c+1}")
end

puts '############################# Situations #################################'
Situation.create(title: "novo")
Situation.create(title: "aprovado")
Situation.create(title: "entregue")
Situation.create(title: "cancelado")

puts '############################# Products #################################'
30.times do |index|
  p = Product.new
  p.code = "#{index+1}"
  p.name = "Produto #{index+1}"
  p.description = "Descrição do Produto #{index+1}"

  rand = Random.rand(10..50)

  p.stock = rand
  p.value = index*rand
  p.productattributes = "Atributos, Attrs, Positivo, Maravilha"
  p.save
  puts "#{p.name}"
end

puts '############################# Orders #################################'
6.times do |index|
  order = Order.new
  order.code = index+1

  daterand = Random.rand(3.years.ago..Time.now)
  order.purchasedate = daterand

  c_rand = Random.rand(1..20)
  order.client_id = c_rand
  
  sit_rand = Random.rand(1..4)
  order.situation_id = sit_rand

  f_rand = Random.rand(1..4)
  order.freightvalue = index*f_rand

  order_rand = Random.rand(1..10)
  order_rand.times do |j|
    order_p = order.orderproducts.build

    p_rand = Random.rand(1..30)
    order_p.product_id = p_rand
    order_p.order_id = index+1
    order_p.quantity = p_rand
    order_p.price = Product.find_by_id(order_p.product_id).value * order_p.quantity
    order_p.created_at = order.purchasedate
    order_p.save
  end

  order.save
  puts "#{order.id}"
end
