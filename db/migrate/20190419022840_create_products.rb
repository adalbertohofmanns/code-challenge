class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.integer :code
      t.string :name
      t.string :description
      t.integer :stock
      t.float :value
      t.string :productattributes

      t.timestamps
    end
  end
end
