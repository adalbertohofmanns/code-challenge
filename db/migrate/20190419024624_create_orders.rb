class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :code
      t.datetime :purchasedate
      t.integer :client_id
      t.integer :situation_id
      t.float :freightvalue

      t.timestamps
    end
  end
end
