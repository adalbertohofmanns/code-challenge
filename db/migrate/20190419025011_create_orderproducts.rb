class CreateOrderproducts < ActiveRecord::Migration[5.2]
  def change
    create_table :orderproducts do |t|
      t.integer :product_id
      t.integer :order_id
      t.integer :quantity
      t.float :price

      t.timestamps
    end
  end
end
