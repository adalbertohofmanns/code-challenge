namespace :db do
  desc "Prepare database"
  task :prepare => :environment do |t, args|
    puts "deletando database"
    Rake::Task["db:drop"].execute
    puts "finish"
    sleep(0.0001)
    puts "criando database"
    Rake::Task["db:create"].execute
    puts "finish"
    sleep(0.0001)
    puts "migrando database"
    Rake::Task["db:migrate"].execute
    puts "finish"
    sleep(0.0001)
    puts "seeding database"
    Rake::Task["db:seed"].execute
    puts "finish prepare"
    sleep(0.0001)
    sh %{ cat README.md }
    sh %{ rails s }

  end
end