Rails.application.routes.draw do
  resources :oorderproducts
  resources :orderproducts
  resources :orders
  resources :products

  get '/tm/:initdate/:duedate', to: 'ticket_medio#report'

end
