class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :update, :destroy]

  # GET /orders
  def index
    @orders = Order.all

    render json: @orders, include: [:orderproducts]
  end

  # GET /orders/1
  def show
    render json: @order, include: [:orderproducts]
  end

  # POST /orders
  def create
    @order = Order.new(order_params)
    @order.purchasedate = Time.now
    @order.code = Order.last.id  + 1
    if @order.save
      
      x = Random.rand(1..5)
      x.times do
        products = Product.where('stock > ?', 0).pluck(:id)
        rand = Random.rand(20..30)
        op = Orderproduct.new
        op.order_id = @order.id
        products_rand = Random.rand(products.min..products.max)
        op.product_id = products_rand
        op.quantity = rand
        product = Product.find_by_id(op.product_id)
        product.decrement(:stock, op.quantity)
        op.price = product.value * op.quantity
        op.created_at = @order.purchasedate
        op.save!
      end
      
      render json: @order, status: :created, location: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end

  end

  # PATCH/PUT /orders/1
  def update
    if @order.update(order_params)
      render json: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /orders/1
  def destroy
    @order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_params
      # params.require(:order).permit(:code, :purchasedate, :client_id, :situation_id, :freightvalue)
      params.permit(:code, :purchasedate, :client_id, :situation_id, :freightvalue)
    end
end
