class TicketMedioController < ApplicationController

  def report
    initdate = Date.parse(params[:initdate], "%m/%d/%Y")
    duedate = Date.parse(params[:duedate], "%m/%d/%Y")
    orders = Order.where(situation_id: 3).where('purchasedate BETWEEN ? AND ?', initdate.beginning_of_day, duedate.end_of_day )
    ops = Orderproduct.where(id: orders).pluck(:price)
    if ops.count < 1
      result = ops.sum + orders.pluck(:freightvalue).sum
    else
      result = ops.sum / ops.count + orders.pluck(:freightvalue).sum
    end
    r = "Ticket médio do periodo de #{params[:initdate]} até #{params[:duedate]} é de #{valor_formatado(result)} com o total de #{orders.count} de #{'venda'.pluralize(orders.count) }".to_json
    render json: { Resultado_TM: r, orders: orders}  
  end

  private

  def helper
    @helper ||= Class.new do
      include ActionView::Helpers::NumberHelper
    end.new
  end

  def valor_formatado(number)
    helper.number_to_currency(number, unit: "R$ ", separator: ",", delimiter: ".")
  end

end
