class Order < ApplicationRecord
  has_many :orderproducts
  belongs_to :client
  belongs_to :situation
end
