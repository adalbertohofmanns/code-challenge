# README

Seu Manuel starts his system 3 years ago.

Things you may want to know:

* Ruby version = 2.6.0

* Configuration
  - run
      bundle install
  - for the first time run
      rake db:prepare "Will create db, migrate, seed and run server"
  - after commando above, run
      rails s

  - fake data - the seed feed the database with some datas, with values and dates aleatory. just to have initial datas. 

* To 'Ticket Médio'
  - on browser
      e.g - http://localhost:3000/tm/initdate/duedate
      for real - http://localhost:3000/tm/01-01-2019/01-02-2019

* Products all
  - on browser
      # GET /products
      http://localhost:3000/products

* Product create
  - on RESTCLIENT
      # POST /products
      http://localhost:3000/products
      try this on the RESTCLIENT - http://localhost:3000/products?name=Teclado Gamer&description=Descrição&stock=22&productattributes=bom, bonito, barato&value=55.0

* Products update
  - on RESTCLIENT
      # PATCH/PUT /products/1
      try this on the RESTCLIENT - http://localhost:3000/products/1?name=Teclado Gamer&description=Descrição&stock=22&productattributes=atributo alterado&value=55.0

* Products delete
  - on browser
      # DELETE /products/1
      http://localhost:3000/products/1






* Database initialization


